/**
 * @file option.cpp
 * @author Thimo Meeusen
 * @brief Implementation of the option class.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "option.h"

option::option()
{
    _text = "";
    _value = 0;
}

option::option(String text, int value)
{
    _text = text;
    _value = value;
}

void option::set_text(String text)
{
    _text = text;
}

String option::get_text(void)
{
    return _text;
}

void option::set_value(int value)
{
    _value = value;
}

int option::get_value()
{
    return _value;
}
