/**
 * @file button.h
 * @author Thimo Meeusen
 * @brief definition of the button class
 * @details class that can be used to add an button to the system.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef button_h
#define button_h

class button
{
public:
    /**
     * @brief Construct a new button::button object
     *
     * @param pin_numb
     */
    button(int pin_numb);

    /**
     * @brief returns the state of the button and reset the button class
     *
     * @return true
     * @return false
     */
    bool check_and_reset();

    /**
     * @brief returns the state of the button.
     *
     * @return true
     * @return false
     */
    bool check_if_pressed();

    /**
     * @brief resets the button class so the button can be pressed again.
     *
     */
    void reset();

private:
    /**
     * @brief interupt handler of the button gpio
     *
     */
    static void interupt_handler();

    /**
     * @brief
     *
     * @param GPIO gpio port of the button
     * @return int index of the gpio port within the button_pressed variable
     */
    static int GPIO_to_index(int GPIO);

    int selected_GPIO; /** GPIO port the button is connected too*/
    int GPIO_index;    /** index of the GPIO port within status array*/
};

#endif // button_h