/**
 * @file display.cpp
 * @author Thimo Meeusen
 * @brief implementation of the display wrapper class
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "display.h"

display::display()
{
    pinMode(Vext, OUTPUT);
    digitalWrite(Vext, HIGH);
    OLED_display = new SSD1306Wire(0x3c, 500000, SDA, SCL, GEOMETRY_128_64, GPIO10);
    OLED_display->init();
    OLED_display->setTextAlignment(TEXT_ALIGN_CENTER);
    OLED_display->setFont(ArialMT_Plain_16);
    clear_screen();
}

void display::set_text(String text)
{
    displayed_text = text;
    refresh();
}

void display::set_border()
{
    border_displayed = true;
    refresh();
}

void display::remove_border()
{
    border_displayed = false;
    refresh();
}

void display::refresh()
{
    OLED_display->clear();
    uint16_t x = OLED_display->width();
    uint16_t y = OLED_display->height();
    OLED_display->drawString(x/2, y/2-10, displayed_text);

    if (border_displayed)
    {
        OLED_display->drawRect(5, 5, x - 10, y - 10);
    }
    OLED_display->display();
}

void display::clear_screen()
{
    displayed_text = "";
    border_displayed = false;
    OLED_display->clear();
}
