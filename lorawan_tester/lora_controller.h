/**
 * @file lora_controller.h
 * @author Sil van Appeldoorn
 * @brief Defenition of lora_controller class to initialise the board and lorawan connection, update if settings have been changed and sending payload
 * @version 0.1
 * @date 2022-03-17
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef lora_controller_H
#define lora_controller_H

#include "Arduino.h"

class lora_controller
{
public:
    /**
     * @brief Construct a new lora controller object
     * Cubecell board initialisation.
     * Lorawan initialisation: Set datarate, region and class
     * Lorawan join
     */
    lora_controller();

    /**
     * @brief Updates lorawan settings when they have been changed
     * Lorawan join
     *
     */
    void update();
    
    /**
     * @brief sends the payload via lorawan
     *
     * @param data payload
     * @param size size of payload
     */
    void send(uint8_t data[], int size);

private:
    /**
     * @brief Get the data rate object value
     *
     * @return int8_t data rate value by checking spreading factor and bandwith
     */
    int8_t get_data_rate();
};

#endif // lora_controller_H