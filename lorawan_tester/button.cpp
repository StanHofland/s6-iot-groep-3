/**
 * @file button.cpp
 * @author Thimo Meeusen
 * @brief implementation of the button class
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <Arduino.h>
#include "button.h"

static bool button_pressed[14] = {false};
static int last_press_time = 0;

button::button(int pin_numb)
{
    selected_GPIO = pin_numb;
    GPIO_index = GPIO_to_index(selected_GPIO);
    button_pressed[GPIO_index] = false;
    PINMODE_INPUT_PULLDOWN(pin_numb);
    attachInterrupt(pin_numb, interupt_handler, RISING);
}

bool button::check_and_reset()
{
    bool return_val = button_pressed[GPIO_index];
    if (return_val)
    {
        reset();
    }
    return return_val;
}

bool button::check_if_pressed()
{
    return button_pressed[GPIO_index];
}

void button::reset()
{
    button_pressed[GPIO_index] = false;
}

void button::interupt_handler()
{
    if ((millis() - last_press_time) > 250)
    {
        button_pressed[0] |= digitalRead(GPIO1);
        button_pressed[1] |= digitalRead(GPIO2);
        button_pressed[2] |= digitalRead(GPIO3);
        button_pressed[3] |= digitalRead(GPIO4);
        button_pressed[4] |= digitalRead(GPIO5);
        button_pressed[5] |= digitalRead(GPIO6);
        button_pressed[6] |= digitalRead(GPIO7);
        button_pressed[7] |= digitalRead(GPIO8);
        button_pressed[8] |= digitalRead(GPIO9);
        button_pressed[9] |= digitalRead(GPIO10);
        button_pressed[10] |= digitalRead(GPIO11);
        button_pressed[11] |= digitalRead(GPIO12);
        button_pressed[12] |= digitalRead(GPIO13);
        button_pressed[13] |= digitalRead(GPIO14);
    }
    last_press_time = millis();
}

int button::GPIO_to_index(int GPIO)
{
    switch (GPIO)
    {
    case (GPIO1):
        return 0;
        break;
    case (GPIO2):
        return 1;
        break;
    case (GPIO3):
        return 2;
        break;
    case (GPIO4):
        return 3;
        break;
    case (GPIO5):
        return 4;
        break;
    case (GPIO6):
        return 5;
        break;
    case (GPIO7):
        return 6;
        break;
    case (GPIO8):
        return 7;
        break;
    case (GPIO9):
        return 8;
        break;
    case (GPIO10):
        return 9;
        break;
    case (GPIO11):
        return 10;
        break;
    case (GPIO12):
        return 11;
        break;
    case (GPIO13):
        return 12;
        break;
    case (GPIO14):
        return 13;
        break;
    default:
        return 0;
    }
}