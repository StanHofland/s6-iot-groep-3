/**
 * @file GPS.h
 * @author Sil van Appeldoorn 
 * @brief Defenition of GPS class
 * @version 0.1
 * @date 2022-03-18
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef GPS_H
#define GPS_H

#include "GPS_Air530Z.h"

class GPS
{
public:
    /**
     * @brief Construct a new GPS object
     *
     */
    GPS();

    /**
     * @brief Trying to get a GPS fix before timeout
     *
     * @return int: -1 if fail, 1 if succesfully gotten a gps fix
     */
    int read();

    /**
     * @brief Get the lon object, longitude value of GPS
     *
     * @return float
     */
    float get_lon();

    /**
     * @brief Get the lat object, latitude value of GPS
     *
     * @return float
     */
    float get_lat();

    /**
     * @brief Get the age object, the age of the GPS 
     *
     * @return int
     */
    int get_age();

    /**
     * @brief Get the satelites object, number of satellites
     *
     * @return int
     */
    int get_satelites();

    /**
     * @brief Get the hdop object value, hdop value of GPS signal
     *
     * @return int
     */
    int get_hdop();

private:
    Air530ZClass _GPS; /**class of GPS chip*/
};

#endif // GPS_H
