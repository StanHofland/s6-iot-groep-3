/**
 * @file list.h
 * @author Thimo Meeusen
 * @brief Universal linked list class
 * @version 1.0
 * @date 2022-02-24
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef list_h
#define list_h

template <class T>
class list
{
    struct node_t
    {
        T *item;
        node_t *next_node;
    };

public:
    /**
     * @brief Construct a new list<T>::list object
     *
     * @details Default constructor for the list class.
     *
     */
    list();

    /**
     * @brief Construct a new list<T>::list object
     *
     * @details Constructor of the list class, can be used to initialize the list with one item.
     *
     * @param object first object to be added to the list.
     */
    list(T *object);

    /**
     * @brief Destroy the list<T>::list object
     *
     * @details Destructor of the list class, cleans all used memory in the class.
     *
     */
    ~list();

    /**
     * @brief Adds an item to the list.
     *
     * @details Creates a node in the list containg the given object and adds the node to the end of the list. returns the index of the node.
     *
     * @param object object to be added to the list.
     * @return int the index of the object in the list.
     */
    int add(T *object);

    /**
     * @brief Removes an item from the list,
     *
     * @details Removes a node at an specific index and returns a pointer to the object contained in the node.
     *
     * @param index index at which to remove the object from the list.
     * @return T* pointer to the removed item.
     */
    T *remove(int index);

    /**
     * @brief Get a spesific item from the list.
     *
     * @details Returns a pointer an item at an specified index.
     *
     * @param index index to the item to get.
     * @return T* pointer to the object
     */
    T *get(int index);

    /**
     * @brief Gives the size of the list.
     *
     * @details Returns the amount of items in the list. The size is equel to the index of the last item in the list.
     *
     * @return int size of the list.
     */
    int size();

private:
    /**
     * @brief Gives node at spisific index.
     *
     * @details Internaly used function used to get a pointer at an specific index.
     *
     * @param index index at which to get the node.
     * @param node_pointer pointer to the node.
     */
    void get_node(int index, node_t **node_pointer);
    node_t *_head; /** first item in the list */
    node_t *_tail; /** last item in the list */
    int _size;     /** size of the list */
};

template <class T>
list<T>::list()
{
    _head = nullptr;
    _tail = nullptr;
    _size = 0;
}

template <class T>
list<T>::list(T *object)
{
    _size = 0;
    add(object);
}

template <class T>
list<T>::~list()
{
    while (_size > 0)
    {
        remove(1);
    }
}

template <class T>
int list<T>::add(T *object)
{
    node_t *new_node = new node_t;
    new_node->item = object;
    new_node->next_node = nullptr;
    if (_size == 0)
    {
        _head = new_node;
        _tail = new_node;
    }
    else
    {
        _tail->next_node = new_node;
        _tail = new_node;
    }
    _size++;
    return _size;
}

template <class T>
T *list<T>::remove(int index)
{
    if ((index < 1) || (index > _size))
    {
        return nullptr;
    }
    node_t *node;
    get_node(index, &node);
    T *item = node->item;
    if (node == _head)
    {
        _head = node->next_node;
    }
    if (node == _tail)
    {
        get_node(index - 1, &_tail);
    }
    delete node;
    _size--;
    return item;
}

template <class T>
T *list<T>::get(int index)
{
    node_t *node = nullptr;
    get_node(index, &node);
    if (node == nullptr)
    {
        Serial.println("ERROR2");
        return nullptr;
    }
    else
    {
        return node->item;
    }
}

template <class T>
int list<T>::size()
{
    return _size;
}

template <class T>
void list<T>::get_node(int index, node_t **node_pointer)
{
    if ((index < 1) || (index > _size))
    {
        *node_pointer = nullptr;
    }
    else
    {
        node_t *node = _head;
        for (int i = 1; i < index; i++)
        {
            node = node->next_node;
        }
        *node_pointer = node;
    }
}

#endif // list_h