/**
 * @file encoder.h
 * @author Sil van Appeldoorbn
 * @brief Defenition of the encoder class to encode latitude, longitude, number of satellites, hdop value and battery percentage
 * @version 0.1
 * @date 2022-03-19
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef encoder_H
#define encoder_H

#include <Arduino.h>

class encoder
{
public:
    /**
    * @brief create empty class
    *
    */
    encoder();

    /**
     * @brief Set the lat object with latitude value of GPS
     *
     * @param lat 
     */
    void set_lat(float lat);

    /**
     * @brief Get the lat object
     *
     * @return float of latitude value
     */
    float get_lat();

    /**
     * @brief Set the lon object with longitude value of GPS
     *
     * @param lon
     */
    void set_lon(float lon);

    /**
     * @brief Get the lon object
     *
     * @return float of longitude value
     */
    float get_lon();

    /**
     * @brief Set the battery object with percentage value of the battery
     *
     * @param battery
     */
    void set_battery(uint8_t battery);

    /**
     * @brief Get the battery object
     *
     * @return uint8_t value of battery in percentage
     */
    uint8_t get_battery();

    /**
     * @brief Set the satalites object with number of satellites
     *
     * @param sattelites
     */
    void set_satalites(uint8_t sattelites);

    /**
     * @brief Get the satalites object
     *
     * @return uint8_t number of satellites
     */
    uint8_t get_satalites();

    /**
     * @brief Set the hdop object with hdop value from GPS
     *
     * @param hdop
     */
    void set_hdop(int hdop);

    /**
     * @brief Get the hdop object
     *
     * @return float value of hdop
     */
    int get_hdop();

    /**
     * @brief encode the payload message
     *
     * @param message
     */
    void encode(uint8_t message[12]);

private:
    float _lat;         /**Value of GPS latitude*/
    float _lon;         /**Value of GPS longitude*/
    uint8_t _battery;   /**Battery percentage*/
    int _hdop;          /**hdop value of GPS*/
    uint8_t _satalites; /**number of satellites*/
};

#endif // encoder_H
