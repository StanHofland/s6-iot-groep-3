/**
 * @file menu_item.h
 * @author Thimo Meeusen
 * @brief Definitions of class menu_item.
 * @details class that can be used te create an item for the menu class. An item consists out of a label and a list of options.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef menu_item_H
#define menu_item_H

#include "Arduino.h"
#include "list.h"
#include "option.h"

class menu_item
{
public:
  /**
   * @brief Construct a new menu item::menu item object.
   *
   * @details Setsup the object and gives the class a label.
   *
   * @param text label of the menu item.
   * @param option_location pointer to the global variable corresponding to the item.
   */
  menu_item(String text, int *option_location);

  /**
   * @brief Destroy the menu item::menu item object
   *
   * @details Deletes all option from the list and destroys the object.
   *
   */
  ~menu_item();

  /**
   * @brief Sets the label of the item.
   *
   * @param text text that has to be set.
   */
  void set_text(String text);

  /**
   * @brief Returns label of the item.
   *
   * @return String the text of the item.
   */
  String get_text();

  /**
   * @brief Returns the current displayed option.
   *
   * @return option* the current displayed option.
   */
  option *get_current_option();

  /**
   * @brief selects the current displayed option as the selcted option.
   *
   */
  void select_current_option();

  /**
   * @brief returns the selected option.
   *
   * @return option* current selected option.
   */
  option *get_selected_option();

  /**
   * @brief Sets the current displayed item to the next item in the list.
   *
   */
  void cycle_forward();

  /**
   * @brief Sets the current displayed item to the previous item in the list.
   *
   */
  void cycle_backwards();

  /**
   * @brief Adds an option to the item and returns the index of the item.
   *
   * @param text text of the option.
   * @param value value of the option.
   * @return int index of the option in the list.
   */
  int add_option(String text, int value);

  /**
   * @brief Deletes an option at a specified index. returns the data of the option.
   *
   * @param index index on which to remove an option.
   * @return option option that has been removed.
   */
  option remove_option(int index);

  /**
   * @brief returnes the size of the list.
   *
   * @return int size of the list.
   */
  int size();

private:
  int *_option_location;      /** pointer to the global variable corresponding to the item */
  String _text;               /** label of the item. */
  int _current_option_pos;    /** position of the corrent diplayed item in the list. */
  option *_current_option;    /** current displayed optiong. */
  option *_selected_option;   /** current selected option. */
  int _amount_of_options;     /** amount of the option in the list. */
  list<option> *_option_list; /** the list containing all the options of the item. */
};

#endif // menu_item_H
