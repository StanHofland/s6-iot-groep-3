/**
 * @file option.h
 * @author Thimo Meeusen
 * @brief definition of the option class.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef option_h
#define option_h

#include "Arduino.h"

class option
{
public:
    /**
     * @brief Construct a new option::option object
     *
     */
    option();

    /**
     * @brief Construct a new option::option object
     *
     * @details Initializes the option with given values.
     *
     * @param text label to be set.
     * @param value value to be set.
     */
    option(String text, int value);

    /**
     * @brief Sets the text of the option.
     *
     * @param text label to be set.
     */
    void set_text(String text);

    /**
     * @brief returns the text of the option.
     *
     * @return String label of the option.
     */
    String get_text(void);

    /**
     * @brief sets the value of the option.
     *
     * @param value value to be set.
     */
    void set_value(int value);

    /**
     * @brief returns the value of the option.
     *
     * @return int value of the option.
     */
    int get_value();

private:
    String _text; /** label of the option. */
    int _value;   /** value of the option */
};

#endif // option_h