/**
 * @file HMI.h
 * @author Thimo Meeusen
 * @brief class that controlls the entire human machine interface.
 * @version 1.0
 * @date 2022-03-02
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "display.h"
#include "button.h"
#include "menu.h"

#ifndef HMI_h
#define HMI_h

class HMI
{
public:
    /**
     * @brief Construct a new HMI object
     *
     */
    HMI();

    /**
     * @brief runs the HMI until the menu is exited
     *
     * @return int returns -1 when no option has been selected and 0 when it has.
     */
    int run();

    /**
     * @brief displays an message on the screen.
     *
     * @details its recomended to use a string with a maximum length of 16 characters.
     *
     * @param message message to be displayed.
     */
    void display_message(String message);

    /**
     * @brief checks if the OK button has been pressed.
     *
     * @return true button is pressed
     * @return false button is not pressed
     */
    bool check_button_press();

private:
    bool _display_items; /** when true the items are displayed on the screan, when false the options are displayed. */
    display *_display;   /** object of the display. */
    button *_B_right;    /** object of the button to cycle to the right*/
    button *_B_left;     /** object of the button to cycle to the left*/
    button *_B_back;     /** object of the back button */
    button *_B_OK;       /** object of the OK button */
    menu *_menu;         /** object of the menu */
};

#endif // HMI_h