/**
 * @file config.h
 * @author Thimo Meeusen
 * @brief config file containg global config variables.
 * @version 1.0
 * @date 2022-03-16
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef config_H
#define config_H

extern int SF; /** spreading factor */
#define SF_7 7
#define SF_8 8
#define SF_9 9
#define SF_10 10
#define SF_11 11
#define SF_12 12

extern int BW; /** bandwith */
#define BW125Khz 125
#define BW250Khz 250

extern int send_flag; /** flag used to determine if a frame has to be send */
#define SET 1
#define RESET 0


#endif // config_H