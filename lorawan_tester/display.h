/**
 * @file display.h
 * @author Thimo Meeusen
 * @brief definition of the display wrapper class
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef display_h
#define display_h

#include <Arduino.h>
#include <Wire.h>
#include <HT_SSD1306Wire.h>

class display
{
public:
    /**
     * @brief Construct a new display::display object
     *
     */
    display();

    /**
     * @brief sets the text displayed on the displayed and refreshes the display.
     *
     * @param text text to be displayed on the screan.
     */
    void set_text(String text);

    /**
     * @brief setts a border around the text.
     *
     */
    void set_border();

    /**
     * @brief removes the border
     *
     */
    void remove_border();

    /**
     * @brief refreshes the display
     *
     */
    void refresh();

    /**
     * @brief clears the display
     *
     */
    void clear_screen();

private:
    SSD1306Wire *OLED_display; /** pointer to (an object of) the class used to control the oled display*/
    String displayed_text;     /** text displayed on the screan */
    bool border_displayed;     /**  says if the border is displayed */
};

#endif // display_h