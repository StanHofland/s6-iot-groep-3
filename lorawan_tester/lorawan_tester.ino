/**
 * @file lorawan_tester.ino
 * @author Thimo Meeusen
 * @brief implementation of HMI class
 * @version 1.0
 * @date 2022-03-16
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "HMI.h"
#include "config.h"
#include "lora_controller.h"
#include "encoder.h"
#include "GPS.h"

HMI *_HMI;
lora_controller *lora;
GPS *_GPS;
encoder _encoder;

void setup() {
  _GPS = new GPS;
  lora = new lora_controller;
  _HMI = new HMI;
  Serial.begin(115200);
  Serial.println("startup");
}

void loop() {
  if (_HMI->check_button_press()) {
    if (_HMI->run() != -1) {
      Serial.print("bandwidth: ");
      Serial.println(BW);
      Serial.print("CR: ");
      Serial.println(SF);
      Serial.print("send data: ");
      Serial.println(send_flag);
      delay(1500);
      _HMI->display_message("");
      if (send_flag == 1) {
        _HMI->display_message("GPS searching");

        while (_GPS->read() != 1) {
          Serial.print("LAT: ");
          Serial.print(_GPS->get_lat(), 6);
          Serial.print(", LON: ");
          Serial.print(_GPS->get_lon(), 6);
          Serial.print(", SATS: ");
          Serial.println(_GPS->get_satelites());
          Serial.print(", HDOP: ");
          Serial.println(_GPS->get_hdop());
        }
        
        _encoder.set_lat(_GPS->get_lat());
        _encoder.set_lon(_GPS->get_lon());
        _encoder.set_satalites(_GPS->get_satelites());
        _encoder.set_hdop(_GPS->get_hdop());
        _encoder.set_battery(100);
        uint8_t message[12] = {0};
        _encoder.encode(message);
        _HMI->display_message("GPS located");
        lora->send(message, 12);
        send_flag = 0;
      } else {
        lora->update();
      }
    }
  }
}
