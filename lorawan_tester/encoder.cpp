/**
 * @file encoder.cpp
 * @author Sil van Appeldoorn
 * @brief Implementation of encoder class
 * @version 0.1
 * @date 2022-03-19
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "encoder.h"

encoder::encoder(){
  _lat = 0;
  _lon = 0;
  _satalites = 0;
  _hdop = 0;
  _battery = 0;
}

void encoder::set_lat(float lat){
  if(lat >= -90 && lat <= 90){
    _lat = lat;
  }
}

float encoder::get_lat(){
    return _lat;
}

void encoder::set_lon(float lon){
  if(lon >= -180 && lon <= 180){
    _lon = lon;
  }
}

float encoder::get_lon(){
    return _lon;
}

void encoder::set_battery(uint8_t battery){
  if(battery <= 100 && battery >= 0){
    _battery = battery;
  }
}

uint8_t encoder::get_battery(){
    return _battery;
}

void encoder::set_satalites(uint8_t sattelites){
  if(sattelites >= 2){
    _satalites = sattelites;
  }
}

uint8_t encoder::get_satalites(){
    return _satalites;
}

void encoder::set_hdop(int hdop){
  _hdop = hdop;
}

int encoder::get_hdop(){
  return _hdop;
}


void encoder::encode(uint8_t message[11]){
    uint32_t send_lat = _lat*1E7;
    uint8_t *puc = (uint8_t *) &send_lat;
    message[0] = puc[3];
    message[1] = puc[2];
    message[2] = puc[1];
    message[3] = puc[0];

    uint32_t send_lon = _lon*1E7;
    puc = (uint8_t *) &send_lon;
    message[4] = puc[3];
    message[5] = puc[2];
    message[6] = puc[1];
    message[7] = puc[0];

    message[8] = _satalites;
    message[9] = (uint8_t)((_hdop*10) & 0xFF);
    message[10] = _battery;
}
