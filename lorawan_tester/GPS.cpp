/**
 * @file GPS.cpp
 * @author Sil van Appeldoorn
 * @brief Implementation of GPS class to get wanted parameters
 * @version 0.1
 * @date 2022-03-29
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "GPS.h"

GPS::GPS()
{
    _GPS.begin();
    _GPS.setmode(MODE_GPS_GALILEO_GLONASS);
}

int GPS::read()
{
    uint32_t start = millis();
    while ((millis() - start) < 120000)
    {
        while (_GPS.available() > 0)
        {
            _GPS.encode(_GPS.read());
        }
        // gps fixed in a second
        if (_GPS.location.age() < 1000)
        {
            return 1;
        }
    }
    return -1;
}

float GPS::get_lon()
{
    return _GPS.location.lng();
}

float GPS::get_lat()
{
    return _GPS.location.lat();
}

int GPS::get_age()
{
    return _GPS.location.age();
}

int GPS::get_satelites()
{
    return _GPS.satellites.value();
}

int GPS::get_hdop()
{
  return _GPS.hdop.hdop();
}
