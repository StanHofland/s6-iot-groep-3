/**
 * @file menu.cpp
 * @author Thimo Meeusen
 * @brief implementation of the menu class.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "menu.h"

menu::menu()
{
  _current_item_pos = 1;
  _current_item = nullptr;
  _amount_of_items = 0;
  _item_list = new list<menu_item>;
}

menu::menu(menu_item *new_item)
{
  _current_item_pos = 1;
  _current_item = nullptr;
  _amount_of_items = 0;
  _item_list = new list<menu_item>(new_item);
}

menu::~menu()
{
  delete _item_list;
}

menu_item *menu::get_current_menu_item()
{
  return _current_item;
}

void menu::cycle_forwards()
{
  if (_amount_of_items <= (_current_item_pos))
  {
    _current_item_pos = 1;
    _current_item = _item_list->get(_current_item_pos);
  }
  else
  {
    _current_item_pos++;
    _current_item = _item_list->get(_current_item_pos);
  }
}

void menu::cycle_backwards()
{
  if (_current_item_pos == 1)
  {
    _current_item_pos = _amount_of_items;
    _current_item = _item_list->get(_current_item_pos);
  }
  else
  {
    _current_item_pos--;
    _current_item = _item_list->get(_current_item_pos);
  }
}

int menu::add_item(menu_item *new_item)
{
  _item_list->add(new_item);
  _amount_of_items++;
  if (_current_item == nullptr)
  {
    _current_item = new_item;
  }
  return _amount_of_items;
}

menu_item *menu::remove_item(int index)
{
  menu_item *old_item = _item_list->get(index);
  _item_list->remove(index);
  return old_item;
}

int menu::size()
{
  return _amount_of_items;
}
