/**
 * @file menu.h
 * @author Thimo Meeusen
 * @brief menu class containg and used to cycle trough all the different menu items.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef menu_H
#define menu_H

#include "Arduino.h"
#include "menu_item.h"

class menu_item;

class menu
{
public:
  /**
   * @brief Construct a new menu::menu object
   *
   */
  menu();

  /**
   * @brief Construct a new menu::menu object
   *
   * @details adds an inittial item to the menu.
   *
   * @param new_item first item to add to the menu.
   */
  menu(menu_item *new_item);

  /**
   * @brief Destroy the menu::menu object
   *
   * @details deletes the menu list.
   *
   */
  ~menu();

  /**
   * @brief returns the current dysplayed item.
   *
   * @return menu_item* current displayed menu item.
   */
  menu_item *get_current_menu_item();

  /**
   * @brief Sets the current displayed item to the next item in the list.
   *
   */
  void cycle_forwards();

  /**
   * @brief Sets the current displayed item to the privious item in the list.
   *
   */
  void cycle_backwards();

  /**
   * @brief adds an item to the menu.
   *
   * @param new_item item to add to the menu.
   * @return int index of item in the menu.
   */
  int add_item(menu_item *new_item);

  /**
   * @brief removes an item from the list and return the pointer to the list.
   *
   * @param index index at which to remove an item.
   * @return menu_item* returns a pointer to the removed item.
   */
  menu_item *remove_item(int index);

  /**
   * @brief returns the amount of items in the list.
   *
   * @return int the size of the menu
   */
  int size();

private:
  int _current_item_pos;       /** position of the curent displayed item in the menu */
  menu_item *_current_item;    /** current displayed item. */
  int _amount_of_items;        /** amount of items in the list. */
  list<menu_item> *_item_list; /** list containing all the items */
};

#endif // menu_H
