/**
 * @file HMI.h
 * @author Thimo Meeusen
 * @brief implementation of class HMI
 * @version 1.0
 * @date 2022-03-02
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "HMI.h"
#include "config.h"

int CR = 0;
int SF = 0;
int BW = 0;
int send_flag = 0;

HMI::HMI()
{
    _display_items = true;

    _display = new display;
    _B_right = new button(GPIO6);
    _B_left = new button(GPIO5);
    _B_back = new button(GPIO3);
    _B_OK = new button(GPIO7);

    _menu = new menu;
    menu_item *item1 = new menu_item("spreading factor", &SF);
    menu_item *item2 = new menu_item("prefered bandwidt", &BW);
    menu_item *item3 = new menu_item("send message", &send_flag);

    item1->add_option("SF:7", SF_7);
    item1->add_option("SF:8", SF_8);
    item1->add_option("SF:9", SF_9);
    item1->add_option("SF:10", SF_10);
    item1->add_option("SF:11", SF_11);
    item1->add_option("SF:12", SF_12);
    item1->select_current_option();

    item2->add_option("125KHz", BW125Khz);
    item2->add_option("250KHz", BW250Khz);
    item2->select_current_option();

    item3->add_option("send", SET);

    _menu->add_item(item1);
    _menu->add_item(item2);
    _menu->add_item(item3);
}

int HMI::run()
{
    _B_back->reset();
    _B_left->reset();
    _B_OK->reset();
    _B_right->reset();
    _display->set_text(_menu->get_current_menu_item()->get_text());
    while (1)
    {
        if (_display_items)
        {
            if (_B_left->check_and_reset())
            {
                _menu->cycle_backwards();
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_text());
            }
            if (_B_right->check_and_reset())
            {
                _menu->cycle_forwards();
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_text());
            }
            if (_B_OK->check_and_reset())
            {
                _display_items = false;
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_current_option()->get_text());
                if (_menu->get_current_menu_item()->get_current_option() == _menu->get_current_menu_item()->get_selected_option())
                {
                    _display->set_border();
                }
                else
                {
                    _display->remove_border();
                }
            }
            if(_B_back->check_and_reset()){
                _display->clear_screen();
                _display->refresh();
                return -1;
            }
        }
        else
        {
            if (_B_left->check_and_reset())
            {
                _menu->get_current_menu_item()->cycle_backwards();
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_current_option()->get_text());
                if (_menu->get_current_menu_item()->get_current_option() == _menu->get_current_menu_item()->get_selected_option())
                {
                    _display->set_border();
                }
                else
                {
                    _display->remove_border();
                }
            }
            if (_B_right->check_and_reset())
            {
                _menu->get_current_menu_item()->cycle_forward();
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_current_option()->get_text());
                if (_menu->get_current_menu_item()->get_current_option() == _menu->get_current_menu_item()->get_selected_option())
                {
                    _display->set_border();
                }
                else
                {
                    _display->remove_border();
                }
            }
            if (_B_OK->check_and_reset())
            {
                _menu->get_current_menu_item()->select_current_option();
                display_message("choice saved");
                _display_items = true;
                return 0;
            }
            if (_B_back->check_and_reset())
            {
                _display_items = true;
                _display->remove_border();
                _display->set_text(_menu->get_current_menu_item()->get_text());
            }
        }
    }
    delay(50);
}

void HMI::display_message(String text)
{
    _display->remove_border();
    _display->set_text(text);
}

bool HMI::check_button_press()
{
    return _B_OK->check_and_reset();
}