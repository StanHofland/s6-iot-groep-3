/**
 * @file menu_item.cpp
 * @author Thimo Meeusen
 * @brief Implementation of class menu_item.
 * @details Class that can be used te create an item for the menu class. An item consists out of a label and a list of options.
 * @version 1.0
 * @date 2022-02-25
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "menu_item.h"
#include "Arduino.h"

menu_item::menu_item(String text, int *option_location)
{
  _option_location = option_location;
  _text = text;
  _current_option_pos = 1;
  _current_option = nullptr;
  _selected_option = nullptr;
  _amount_of_options = 0;
  _option_list = new list<option>();
}

menu_item::~menu_item()
{
  delete _option_list;
  while (_amount_of_options > 0)
  {
    remove_option(1);
  }
}

void menu_item::set_text(String text)
{
  _text = text;
}

String menu_item::get_text()
{
  return _text;
}

option *menu_item::get_current_option()
{
  return _current_option;
}

void menu_item::cycle_forward()
{
  if (_amount_of_options <= (_current_option_pos))
  {
    _current_option_pos = 1;
    _current_option = _option_list->get(_current_option_pos);
  }
  else
  {
    _current_option_pos++;
    _current_option = _option_list->get(_current_option_pos);
  }
}

void menu_item::cycle_backwards()
{
  if (_current_option_pos == 1)
  {
    _current_option_pos = _amount_of_options;
    _current_option = _option_list->get(_current_option_pos);
  }
  else
  {
    _current_option_pos--;
    _current_option = _option_list->get(_current_option_pos);
  }
}
int menu_item::add_option(String text, int value)
{
  option *new_option = new option(text, value);
  _option_list->add(new_option);
  _amount_of_options++;
  if (_current_option == nullptr)
  {
    _current_option = new_option;
  }
  return _amount_of_options;
}

option menu_item::remove_option(int index)
{
  option *old_option = _option_list->remove(index);
  option returnval = *old_option;
  delete old_option;
  return returnval;
}

int menu_item::size()
{
  return _amount_of_options;
}

void menu_item::select_current_option()
{
  _selected_option = _current_option;
  *_option_location = _selected_option->get_value();
}

option *menu_item::get_selected_option()
{
  return _selected_option;
}
