/**
 * @file lora_controller.cpp
 * @author Sil van Appeldoorn
 * @brief Implementation of lora_controller class with custom devEui, appEui, appKey and settings
 * @version 0.1
 * @date 2022-03-17
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "lora_controller.h"
#include "LoRaWan_APP.h"
#include "config.h"

uint8_t devEui[] = {0x60, 0x81, 0xF9, 0x0C, 0xA8, 0x40, 0x08, 0x5B};
uint8_t appEui[] = {0x60, 0x81, 0xF9, 0xCB, 0x8F, 0xB8, 0x73, 0xE8};
uint8_t appKey[] = {0xBA, 0xD5, 0x5C, 0xDC, 0x95, 0x38, 0x23, 0x5E, 0x67, 0x57, 0xAE, 0x07, 0xA7, 0xC8, 0x84, 0x94};

/* ABP para*/
uint8_t nwkSKey[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t appSKey[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint32_t devAddr = (uint32_t)0x00000000;
uint16_t userChannelsMask[6] = {0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000}; ///< LoraWan channelsmask, default channels 0-7
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;                                   ///< LoraWan region, select in arduino IDE tools
DeviceClass_t loraWanClass = LORAWAN_CLASS;                                      ///< LoraWan Class, Class A and Class C are supported
uint32_t appTxDutyCycle;                                                         ///< the application data transmission duty cycle.  value in [ms].
bool overTheAirActivation = LORAWAN_NETMODE;                                     ///< OTAA or ABP, OTAA is prefered
uint8_t appPort = 2;                                                             ///< Application port to be used by sending.
bool loraWanAdr = LORAWAN_ADR;                                                   ///< ADR enable
bool keepNet = LORAWAN_NET_RESERVE;                                              ///< set LORAWAN_Net_Reserve ON, the node could save the network info to flash, when node reset not need to join again
bool isTxConfirmed = LORAWAN_UPLINKMODE;                                         ///<  Indicates if the node is sending confirmed or unconfirmed messages
uint8_t confirmedNbTrials = 4;                                                   ///< number of trials to send.

lora_controller::lora_controller()
{
    boardInitMcu();
    LoRaWAN.setDataRateForNoADR(get_data_rate());
    LoRaWAN.init(loraWanClass, loraWanRegion);
    LoRaWAN.join();
}

void lora_controller::update()
{
    LoRaWAN.setDataRateForNoADR(get_data_rate());
    LoRaWAN.init(loraWanClass, loraWanRegion);
    LoRaWAN.join();
}

void lora_controller::send(uint8_t data[], int size)
{
    appDataSize = size;
    for (int i = 0; i < size; i++)
    {
        appData[i] = data[i];
    }
    appData[size] = NULL;
    LoRaWAN.send();
}

int8_t lora_controller::get_data_rate()
{
    if (SF == SF_7)
    {
        if (BW == BW250Khz)
        {
            return 6;
        }
    }
    return 12 - SF;
}