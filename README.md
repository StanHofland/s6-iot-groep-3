# S6-IOT-groep 3



## Getting started

To use the LoraWAN Tester, the repository should be downloaded and opened in arduino ide pro.
https://www.arduino.cc/en/software#:~:text=Future%20Version%20of%20the%20Arduino%20IDE

In arduino ide pro click: File -> preferences, additional board managers and add: https://resource.heltec.cn/download/package_CubeCell_index.json.

In tools -> Board manager type "cubecell" and click install.

In Tools -> Board, select the Cubecell-GPS -> HTCC-AB02S.

## Configuration

In Tools change the settings to the following values:

Adr -> Off

Atsupport -> Off

Class -> Class A

Debuglevel -> Freq & dio

Netmode -> OTAA

Region -> EU868 (For EU)

Uplink -> Unconfirmed

## Add your credentials

In the Helium console create a device and in lora_controller.cpp change the devEui[], appEui and appKey[] to the values in the Helium console.


## Compile and upload

Now you can Compile the code and upload it to your board.

## Hardware setup

Connect 4 buttons according to the connection diagram below.

![afbeelding-1.png](./afbeelding-1.png)

An external GPS antenna is required.

https://www.kiwi-electronics.nl/nl/gps-antenne-externe-actieve-antenne-3-5v-28db-5-meter-sma-620?search=gps%20antenne

https://www.kiwi-electronics.nl/nl/sma-naar-ufl-u-fl-ipx-ipex-rf-kabel-619
